import time

from selenium.webdriver import Chrome
from selenium.webdriver import Edge
from selenium.webdriver.chrome.service import Service
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from webdriver_manager.chrome import ChromeDriverManager


# Test - uruchomienie Chroma
def test_my_first_chrome_selenium_test():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1280, 720)

    # Otwarcie strony testareny - pierwsze użycie Selenium API
    browser.get('http://demo.testarena.pl/zaloguj')

    # Weryfikacja czy tytuł otwartej strony zawiera w sobie 'TestArena'
    assert 'TestArena' in browser.title

    time.sleep(10)
    # Zamknięcie przeglądarki
    browser.quit()


def test_allegro():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1280, 720)

    # Otwarcie strony testareny - pierwsze użycie Selenium API
    browser.get('https://allegro.pl/')

    # Weryfikacja czy tytuł otwartej strony zawiera w sobie 'TestArena'
    assert 'allegro' in browser.title

    time.sleep(10)
    # Zamknięcie przeglądarki
    browser.quit()


def test_awesome_testing():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1280, 720)

    # Otwarcie strony testareny - pierwsze użycie Selenium API
    browser.get('http://www.awesome-testing.blogspot.com/')

    # Weryfikacja czy tytuł otwartej strony zawiera w sobie 'TestArena'
    assert 'Awesome Testing' in browser.title

    # Żeby dodać ciastko musimy najpierw wejsc na daną srtonę
    browser.add_cookie({"name": "displayCookieNotice", "value": "y"})
    browser.refresh()

    time.sleep(10)
    # Zamknięcie przeglądarki
    browser.quit()


# Test - uruchomienie Edge
def test_my_first_edge_selenium_test():
    # Uruchomienie przeglądarki Edge. Ścieżka do geckodrivera (drivera dla Firefoxa)
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    service = Service(EdgeChromiumDriverManager().install())
    browser = Edge(service=service)

    # Otwarcie strony www.google.pl

    # Weryfikacja tytułu

    # Zamknięcie przeglądarki
    browser.quit()
