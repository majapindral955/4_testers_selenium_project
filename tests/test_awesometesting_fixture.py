import re
import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.chrome import service
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


@pytest.fixture()
def browser():
    # częsć która wykona się przed każdym testem
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    browser.get('http://www.awesome-testing.blogspot.com/')

    # cos co przekazujemy do każdego testu
    yield browser

    # czeęsć która wykona się po każdym tescie
    browser.quit()


def test_post_count(browser):
    posts = browser.find_elements(By.CSS_SELECTOR, ".post-title")
    assert len(posts) == 4


def test_post_count_after_search(browser):
    browser.find_element(By.CSS_SELECTOR, ".gsc-input input").send_keys("Selenium")
    browser.find_element(By.CSS_SELECTOR, ".gsc-search-button input").click()
    time.sleep(5)
    posts = browser.find_elements(By.CSS_SELECTOR, ".post-title")
    assert len(posts) == 20


def test_post_count_after_clicking_year_label(browser):
    browser.find_element(By.LINK_TEXT, "2019").click()
    time.sleep(5)
    posts = browser.find_elements(By.CSS_SELECTOR, ".post-title")
    expected_numer_of_posts = 10
    assert len(posts) == expected_numer_of_posts
