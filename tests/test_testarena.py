import time
import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

administrator_email = "administrator@testarena.pl"


@pytest.fixture()
def browser():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1280, 720)
    browser.get('http://demo.testarena.pl/zaloguj')
    browser.find_element(By.CSS_SELECTOR, "#email").send_keys("administrator@testarena.pl")
    browser.find_element(By.CSS_SELECTOR, "#password").send_keys("sumXQQ72$L")
    browser.find_element(By.CSS_SELECTOR, "#login").click()

    yield browser
    browser.quit()


def test_testarena_login(browser):
    # administrator_email = "administrator@testarena.pl"
    # service = Service(ChromeDriverManager().install())
    # browser = Chrome(service=service)
    # browser.set_window_size(1280, 720)
    # browser.get('http://demo.testarena.pl/zaloguj')

    # Logowanie
    # emailImput = browser.find_element(By.CSS_SELECTOR, "#email")
    # emailImput.send_keys(administrator_email)
    # passwordImput = browser.find_element(By.CSS_SELECTOR, "#password")
    # passwordImput.send_keys("sumXQQ72$L")
    # submitButton = browser.find_element(By.CSS_SELECTOR, "#login")
    # submitButton.click()

    # To samo zapisane inaczej (bardziej zalecane)
    # browser.find_element(By.CSS_SELECTOR, "#email").send_keys("administrator@testarena.pl")
    # browser.find_element(By.CSS_SELECTOR, "#password").send_keys("sumXQQ72$L")
    # browser.find_element(By.CSS_SELECTOR, "#login").click()

    # Asercja
    user_email = browser.find_element(By.CSS_SELECTOR, ".user-info small").text
    assert user_email == administrator_email


# lub
# assert browser.find_element(By.CSS_SELECTOR, ".user-info small").text == administrator_email

def test_logout(browser):
    browser.find_element(By.CSS_SELECTOR, ".icons-switch").click()
    assert 'zaloguj' in browser.current_url
    assert browser.find_element(By.CSS_SELECTOR, "#password").is_displayed()


def test_add_message(browser):
    browser.find_element(By.CSS_SELECTOR, ".icon_mail").click()

    # kandtruktor przyjmuje dwa parametry: nasz obiekt przeglądarki/drivera i maksymalny czas czekania
    wait = WebDriverWait(browser, 10)
    # Pythonowa tupla / krotki
    message_area = (By.CSS_SELECTOR, "#j_msgContent")
    wait.until(expected_conditions.element_to_be_clickable(message_area))

    browser.find_element(By.CSS_SELECTOR, "#j_msgContent").send_keys("Wiadomosć")

    # time.sleep(10)
    # browser.quit()


def test_search_project(browser):
    browser.find_element(By.CSS_SELECTOR, ".icon_tools").click()
    browser.find_element(By.CSS_SELECTOR, "#search").send_keys("kamil")
    browser.find_element(By.CSS_SELECTOR, "#j_searchButton").click()

    projects = browser.find_elements(By.CSS_SELECTOR, ".t_number")
    assert len(projects) > 0
