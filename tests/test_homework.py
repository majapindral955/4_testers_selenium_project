import random
import string
import time

import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

from pages.arena.home_page import HomePage
from pages.arena.login_page import LoginPage
from pages.arena.project_page import ProjectPage

administrator_email = "administrator@testarena.pl"


def get_random_string(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))


@pytest.fixture()
def browser():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1280, 720)
    login_page = LoginPage(browser)
    login_page.visit()
    login_page.login(administrator_email, "sumXQQ72$L")

    yield browser
    browser.quit()


def test_creating_a_new_project(browser):
    home_page = HomePage(browser)
    home_page.click_on_administrator_link()

    project_page = ProjectPage(browser)
    project_page.find_add_project_element()

    project_name = get_random_string(10)
    prefix = get_random_string(6)
    browser.find_element(By.CSS_SELECTOR, "#name").send_keys(project_name)
    browser.find_element(By.CSS_SELECTOR, "#prefix").send_keys(prefix)
    browser.find_element(By.CSS_SELECTOR, "#description").send_keys("Projekt selenium")
    browser.find_element(By.CSS_SELECTOR, "#description").send_keys("Projekt selenium")
    browser.find_element(By.CSS_SELECTOR, "#save").click()


def test_new_project(browser):
    home_page = HomePage(browser)
    home_page.click_on_administrator_link()

    project_page = ProjectPage(browser)
    project_page.search_added_project()
    project_page.verify_added_project("S3KBK5EYMI")
