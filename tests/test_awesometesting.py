import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.chrome import service
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_post_count():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    # Otwarcie strony
    browser.get('http://www.awesome-testing.blogspot.com/')

    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, ".post-title")

    # Asercja że lista ma 4 elementy
    assert len(posts) == 4

    # Zamknięcie przeglądarki
    time.sleep(5)
    browser.quit()


def test_post_count_after_search():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    # Otwarcie strony
    browser.get('http://www.awesome-testing.blogspot.com/')

    # Szukanie
    browser.find_element(By.CSS_SELECTOR, ".gsc-input input").send_keys("Selenium")
    browser.find_element(By.CSS_SELECTOR, ".gsc-search-button input").click()
    # Czekanie na stronę
    time.sleep(5)

    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, ".post-title")

    # Asercja że lista ma 20 elementów
    assert len(posts) == 20

    # Zamknięcie przeglądarki
    time.sleep(5)
    browser.quit()


def test_post_count_after_clicking_year_label():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    # Otwarcie strony
    browser.get('http://www.awesome-testing.blogspot.com/')

    # Inicjalizacja elementu 2019
    browser.find_element(By.LINK_TEXT, "2019").click()

    # Kliknięcie na 2019

    # Czekanie na stronę
    time.sleep(5)

    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, ".post-title")

    # Asercja że lista ma 10 element
    expected_numer_of_posts = 10
    assert len(posts) == expected_numer_of_posts

    # Zamknięcie przeglądarki
    time.sleep(5)
    browser.quit()
