from selenium.webdriver.common.by import By


# logout_icon = "icons-switch" i wtedy:
# self.browser.find_element(By.CSS_SELECTOR, self.logout_icon).click()

class HomePage:

    def __init__(self, browser):
        self.browser = browser

    def click_logout(self):
        self.browser.find_element(By.CSS_SELECTOR, ".icons-switch").click()

    def click_on_administrator_link(self):
        self.browser.find_element(By.CSS_SELECTOR, ".icon_tools").click()
