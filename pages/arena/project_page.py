from selenium.webdriver.common.by import By


# search_term = "kamil"


class ProjectPage:

    def __init__(self, browser):
        # self.find_a_projets_with_kamil_word = None
        self.browser = browser

    def search_for_project(self, search_term):
        self.browser.find_element(By.CSS_SELECTOR, "#search").send_keys(search_term)
        self.browser.find_element(By.CSS_SELECTOR, "#j_searchButton").click()

    def verify_project_found(self, search_term):
        projects = self.browser.find_elements(By.CSS_SELECTOR, "tbody tr")
        assert len(projects) > 0

        names = self.browser.find_elements(By.CSS_SELECTOR, 'tbody tr td:nth-of-type(1)')
        for name in names:
            assert search_term in name.text.lower()

    def find_add_project_element(self):
        add_projects = self.browser.find_elements(By.CSS_SELECTOR, '.button_link_li')
        add_projects[0].click()

    def search_added_project(self):
        self.browser.find_elements(By.CSS_SELECTOR, '.button_link_li')

    def verify_added_project(self, search_term):
        self.browser.find_element(By.CSS_SELECTOR, "#search").send_keys(search_term)
        self.browser.find_element(By.CSS_SELECTOR, "#j_searchButton").click()


